const { join } = require('path');

const eslint = require('eslint');
const yaml = require('js-yaml');
const { fs, vol } = require('memfs');

/**
 * @param {object[]} testEnv - A mapping of environment variables to use in the test.
 * @param {object} gitlabCI - An object which stubs the content of a `.gitlab-ci.yml` file.
 * @param {string} gitlabCIFile - The location of the GitLab CI stub file.
 * @returns {Function} The patched module.
 */
function setup(testEnv, gitlabCI, gitlabCIFile = '/build/.gitlab-ci.yml') {
  process.env = testEnv;
  jest.restoreAllMocks();
  jest.resetModules();
  jest.spyOn(process, 'cwd').mockReturnValue('/build');
  jest.setMock('eslint', eslint);
  jest.setMock('fs', fs);
  vol.reset();
  vol.fromJSON({
    [gitlabCIFile]: yaml.dump(gitlabCI),
  });
  // eslint-disable-next-line node/global-require
  return require('.');
}

it('should write a code quality report', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
        { line: 43, message: 'This is a linting warning', ruleId: 'linting-warning', severity: 1 },
      ],
    },
  ]);
  expect(JSON.parse(vol.readFileSync('/build/output.json'))).toStrictEqual([
    {
      description: 'This is a linting error',
      fingerprint: '2235367ffc3cbeb18474a7285f9d5803',
      location: { lines: { begin: 42 }, path: 'filename.js' },
      severity: 'major',
    },
    {
      description: 'This is a linting warning',
      fingerprint: 'f86c96fb4ffdf46ae37fceed0d317715',
      location: { lines: { begin: 43 }, path: 'filename.js' },
      severity: 'minor',
    },
  ]);
});

it('should throw if the output location is empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: '' } } },
    },
  );
  expect(() => formatter([])).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but no value was found.',
    ),
  );
});

it('should throw if the output location is an array', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: [] } } },
    },
  );
  expect(() => formatter([])).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but found an array instead.',
    ),
  );
});

it('should not fail if a rule id is null', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      messages: [{ line: 42, message: 'This is a linting error', ruleId: null, severity: 2 }],
    },
  ]);
  expect(JSON.parse(vol.readFileSync('/build/output.json'))).toStrictEqual([
    {
      description: 'This is a linting error',
      fingerprint: '8e905c6c557d0066e5a01827b6216be6',
      location: { lines: { begin: 42 }, path: 'filename.js' },
      severity: 'major',
    },
  ]);
});

it('should skip the output if CI_JOB_NAME is not defined', () => {
  const formatter = setup(
    {},
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([]);
  expect(vol.existsSync('/build/output.json')).toBe(false);
});

it('should respect the ESLINT_CODE_QUALITY_REPORT environment variable', () => {
  const formatter = setup(
    {
      ESLINT_CODE_QUALITY_REPORT: 'elsewhere.json',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([]);
  expect(JSON.parse(vol.readFileSync('/build/elsewhere.json'))).toStrictEqual([]);
});

it('should return the value of the default reporter', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
      ],
    },
  ]);
  expect(result).toBe(`
/build/filename.js
  42:0  error  This is a linting error  linting-error

✖ 1 problem (1 error, 0 warnings)
`);
});

it('should delegate the output using the ESLINT_FORMATTER environment variable', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      ESLINT_FORMATTER: 'custom-formatter',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const customFormatter = jest.fn().mockReturnValue('ESLint output');
  jest.spyOn(eslint.CLIEngine, 'getFormatter').mockReturnValue(customFormatter);
  const result = formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      messages: [{ line: 42, message: 'This is a linting error', ruleId: 'linting-error' }],
    },
  ]);
  expect(eslint.CLIEngine.getFormatter).toHaveBeenCalledWith('custom-formatter');
  expect(result).toBe('ESLint output');
});

it('should fallback to stylish if the formatter itself is specified as output formatter', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      ESLINT_FORMATTER: require.resolve('.'),
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
      ],
    },
  ]);
  expect(result).toBe(`
/build/filename.js
  42:0  error  This is a linting error  linting-error

✖ 1 problem (1 error, 0 warnings)
`);
});

it('should throw if CI_CONFIG_PATH is empty', () => {
  const formatter = setup(
    {
      CI_CONFIG_PATH: '',
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  expect(() => formatter([])).toThrow(
    new Error(
      'Could not resolve .gitlab-ci.yml to automatically detect report artifact path.' +
        ' Please manually provide a path via the ESLINT_CODE_QUALITY_REPORT variable.',
    ),
  );
});
